import React, { Component } from 'react';
import './App.css';
import Footer from './components/Footer';
import { Button } from 'antd';
import LocationDisplay from './components/LocationDisplay';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { showLocation: false };
  }

  componentDidMount = () => {
    navigator.permissions.query({ name: 'geolocation' })
      .then(({ state }) => {
        if (state === 'granted') {
          this.setState({ showLocation: true });
        }
      });
  }

  get requestLocationButton() {
    const onSubmit = (evt) => {
      evt.preventDefault();
      this.setState({ showLocation: true });
    };
    return (<form onSubmit={onSubmit} className={'text-content'}>
      <span>
        Find your nearest physical address from your mobile device's location services.  Invaluable when when calling emergency services from an unfamiliar place.  We respect your privacy, this app is hosted entirely in your browser and never 'calls home' with your location data.  The source is open-source hosted on <a href={'https://gitlab.com/codeocelot/address-alert'}>GitLab</a>
      </span>
      <div>
        <Button htmlType={'submit'} type={'primary'}>Get Location</Button>
      </div>

    </form>);
  }

  get showProgressThenLocation() {
    return (
      <LocationDisplay />
    );
  }

  render() {
    return (
      <div className="App">
        <div className={'content'}>
          <header className="App-header">
            <h1 className="App-title">myaddr.io</h1>
          </header>

          <div>
            {
              this.state.showLocation
                ? this.showProgressThenLocation
                : this.requestLocationButton
            }
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
