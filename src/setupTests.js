const mockGeolocation = {
  getCurrentPosition: jest.fn(),
  watchPosition: jest.fn()
};

const mockPermissions = {
  query: () => Promise.resolve({ state: 'denied' }),
}

global.navigator.geolocation = mockGeolocation;
global.navigator.permissions = mockPermissions;