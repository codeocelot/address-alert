module.exports = {
  gmaps: process.env.REACT_APP_GMAPS_KEY,
  what3Words: process.env.REACT_APP_WHAT3WORDS_KEY,
};
