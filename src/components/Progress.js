import React from 'react';
import PropTypes from 'prop-types';
import { Progress as AntdProgress } from 'antd';

class Progress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accumulated: 0,
    };
  }
  componentDidMount() {
    this.startCount();
  }
  startCount = () => {
    const intervalId = setInterval(
      () => {
        console.log('tick ', this.state.accumulated);
        if (this.state.accumulated >= 1) {
          clearInterval(intervalId);
          this.props.onFinish();
        } else this.increment(0.01);
      },
      50,

    );
  }
  increment = (val) => this.setState({
    accumulated: this.state.accumulated + val,
  })
  render() {
    return this.state.accumulated <= 1
      && <AntdProgress
        showInfo={false}
        percent={this.state.accumulated * 100}
      />;
  }
}

Progress.propTypes = {
  onStart: PropTypes.func.isRequired,
  onFinish: PropTypes.func,
};

Progress.defaultProps = {
  onFinish: () => {},
};

export default Progress;
