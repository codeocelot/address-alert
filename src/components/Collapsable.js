import React from 'react';
import PropTypes from 'prop-types';

const Collapsable = (WrappedComponent) => ({ isCollapsed, ...rest }) => (
  <div style={{ display: this.props.isCollapsed ? 'none' : 'initial' }}><WrappedComponent {...rest} /></div>
);

Collapsable.propTypes = {
  isCollapsed: PropTypes.bool,
};

Collapsable.defaultProps = {
  isCollapsed: true,
};

export default Collapsable;
