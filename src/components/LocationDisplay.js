import React, { Component } from 'react';
import PropTypes from 'prop-types';
import secrets from '../secrets';
import { geolocated } from 'react-geolocated';
import Map from './Map';
import GeolocationUnavailable from './GeolocationUnavailableDisplay';
import GeolocationDisabled from './GeolocationDisabled';

const getRevLookupUrl = (lat, lng) => `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${secrets.gmaps}`;

class LocationDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentDidUpdate = (prevProps) => {
    if (!prevProps.coords && this.props.coords) {
      Promise.all([this.getWhat3Words(), this.getUserAddress()]);
    }
  }

  getUserAddress = () => {
    const lat = this.props.coords.latitude;
    const lng = this.props.coords.longitude;
    return fetch(getRevLookupUrl(lat, lng))
      .then((response) => response.json())
      .then(({ results }) => this.setState({ location: results[0] }))
      .catch((error) => this.setState({ error }));
  }

  getWhat3Words = () => {
    const coords = [
      this.props.coords.latitude,
      this.props.coords.longitude,
    ].join(',');
    return fetch(
      `https://api.what3words.com/v2/reverse?key=${secrets.what3Words}&coords=${coords}`,
    )
      .then((r) => r.json())
      .then(({ words }) => this.setState({ words }))
      .catch((error) => this.setState({
        error,
      }));
  }

  maybeGetPrettyName = () => this.state.location
    && this.state.location.formatted_address;

  maybeGetLatLong = () => this.props.coords && [
    this.props.coords.latitude,
    this.props.coords.longitude,
  ].map((num) => num.toFixed(4))
    .join(', ')

  get what3wordsLink() {
    return this.state.words && `https://map.what3words.com/${this.state.words}`;
  }

  get locationServices() {
    const lat = this.props.coords.latitude;
    const lng = this.props.coords.longitude;
    return (<div className="">
      <div className="grid-wrapper text-content">
        <div className={'col-label'}> location: </div>
        <div> { this.maybeGetPrettyName() } </div>
        <div className={'col-label'}> lat, long: </div>
        <div> { this.maybeGetLatLong() } </div>
        <div className={'col-label'}>
          <a href={this.what3wordsLink} >what3words:</a>
        </div>
        <div> { this.state.words } </div>
      </div>
      <Map
        isMarkerShown
        position={{ lat, lng }}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${secrets.gmaps}`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '600px' }} />}
        mapElement={<div style={{ height: '100%' }} />}
      />
    </div>);
  }

  render() {
    let lat = null;
    let lng = null;
    if (this.props.coords) {
      lat = this.props.coords.latitude;
      lng = this.props.coords.longitude;
    }
    return (<div className="">
      {this.props.isGeolocationAvailable === false && <GeolocationUnavailable/>}
      {this.props.isGeolocationEnabled === false && <GeolocationDisabled/>}
      <div className="grid-wrapper">
        <div className={'col-label'}> location: </div>
        <div> { this.maybeGetPrettyName() } </div>
        <div className={'col-label'}> lat, long: </div>
        <div> { this.maybeGetLatLong() } </div>
        <div className={'col-label'}>
          <a href={this.what3wordsLink} >what3words:</a>
        </div>
        <div> { this.state.words } </div>
      </div>
      <Map
        isMarkerShown
        position={{ lat, lng }}
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${secrets.gmaps}`}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '600px' }} />}
        mapElement={<div style={{ height: '100%' }} />}
      />
    </div>);
  }
}

LocationDisplay.propTypes = {
  coords: PropTypes.shape({
    latitude: PropTypes.string,
    longitude: PropTypes.string,
  }),
  isGeolocationAvailable: PropTypes.bool,
  isGeolocationEnabled: PropTypes.bool,
};

LocationDisplay.defaultProps = {
  coords: {},
  isGeolocationAvailable: undefined,
  isGeolocationEnabled: undefined,
};

const geolocationProvider = navigator.geolocation;
geolocationProvider.getCurrentPosition = geolocationProvider.watchPosition;

export default geolocated({ geolocationProvider })(LocationDisplay);
