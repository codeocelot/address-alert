import React from 'react';

export default () => (
  <div>
    <span>Please enable us to use geolocation in your browser</span>
  </div>
);
