import React from 'react';

export default () => (
  <div>
    <span>
      It looks like your browser does not support Geolocation technology.  Please ensure your browser is up-to-date.
    </span>
  </div>
)