import React from 'react';
import './Footer.css';

export default () => (<footer className={'footer'}>
  <span>Made with &lt;3 for each other</span>
  <div>
    <a href={"https://gitlab.com/codeocelot/address-alert"}>View Source</a>
  </div>
</footer>
);
