import React from 'react';
import PropTypes from 'prop-types';


class ShowProgress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }
  onFinish = () => this.setState({ loading: false })
  render() {
    return this.state.loading
      ? React.cloneElement(this.props.progress, {onFinish: this.onFinish})
      : this.props.children;
  }
}

ShowProgress.propTypes = {
  progress: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
}

export default ShowProgress;