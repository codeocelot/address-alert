import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { GoogleMap, Marker, withScriptjs, withGoogleMap } from 'react-google-maps';

class Map extends Component {
  render() {
    return (
      <GoogleMap
        defaultZoom={19}
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '400px' }} />}
        mapElement={<div style={{ height: '100%' }} />}
        center={this.props.position}
      >
        {this.props.isMarkerShown && <Marker position={this.props.position} />}
      </GoogleMap>
    );
  }
}

Map.propTypes = {
  isMarkerShown: PropTypes.bool,
  position: PropTypes.shape({
    lat: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    lng: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }).isRequired,
};

Map.defaultProps = {
  isMarkerShown: false,
};

export default withScriptjs(withGoogleMap(Map));
